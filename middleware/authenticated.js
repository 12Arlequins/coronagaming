export default function ({ store, redirect, app}) {
    if (!store.getters['auth/connected']) {
        return redirect(app.localePath('/'));
    }
}