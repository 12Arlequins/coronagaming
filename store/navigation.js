export const state = () => ({
    authenticationMenuVisible: false
});
  
export const mutations = {
    NAV_TIGGER_AUTH(state) {
        state.authenticationMenuVisible = !state.authenticationMenuVisible;
    }
}

export const actions = {
    tiggerAuthenticationMenu({ commit }) {
        commit('NAV_TIGGER_AUTH');
    }
}

export const getters = {
    showAuthenticationMenu: (state) => state.authenticationMenuVisible,
}