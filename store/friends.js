export const state = () => ({
    friends: null,
});

export const mutations = {
    INITALIZE_FRIENDS: Object.assign,
    FETCH_FRIENDS(state, allUsers) {
        console.log('friends::FETCH_FRIENDS', allUsers);
        state.friends = allUsers;
    },
}

export const actions = {
    fetchFriends({ commit, rootState }, {callback}) {
        console.log('friends::fetchFriends');
        this.$axios.get(`users.json?auth=${rootState.auth.idToken}`)
            .then((response) => {
                commit('FETCH_FRIENDS', {
                    friends: response,
                });
                callback();
            }).catch(() => {
                callback(['friends.error.fetchUser']);
            });
    },
    sendInvite({ dispatch, rootState}, {friendUuid}) {
        console.log('friends::sendInvite');
        var invitingRequest = this.$axios.put(`users/${friendUuid}/inviting/${rootState.auth.idToken}.json?auth=${rootState.auth.idToken}`, true);
        var invitedRequest = this.$axios.put(`users/${rootState.auth.idToken}/invited/${friendUuid}.json?auth=${rootState.auth.idToken}`, true);
        Promise.all([invitingRequest, invitedRequest]).catch((responses) => {
            console.log('friends::sendInvite::failed', responses);
            // @TODO Manage Error, may be juste display a toast
        });
        dispatch('user/fetchUser', null, {root:true});
    },
    acceptInvite({ dispatch, rootState}, {friendUuid}) {
        console.log('friends::acceptInvite');
        var addingFriendsToHim = this.$axios.put(`users/${friendUuid}/friends/${rootState.auth.idToken}.json?auth=${rootState.auth.idToken}`, true);
        var addingFriendsToMe = this.$axios.put(`users/${rootState.auth.idToken}/friends/${friendUuid}.json?auth=${rootState.auth.idToken}`, true);
        Promise.all([addingFriendsToHim, addingFriendsToMe]).then(() => {
            var invitingRequest = this.$axios.delete(`users/${friendUuid}/inviting/${rootState.auth.idToken}.json?auth=${rootState.auth.idToken}`, true);
            var invitedRequest = this.$axios.delete(`users/${rootState.auth.idToken}/invited/${friendUuid}.json?auth=${rootState.auth.idToken}`, true);
            // @TODO Manage Error.
        }).catch((responses) => {
            console.log('friends::acceptInvite::failed', responses);
            // @TODO Manage Error, may be juste display a toast
        });
        dispatch('user/fetchUser', null, {root:true});
    },
    declineInvite({ dispatch, rootState}, {friendUuid}) {
        console.log('friends::declineInvite');
        var invitingRequest = this.$axios.delete(`users/${friendUuid}/inviting/${rootState.auth.idToken}.json?auth=${rootState.auth.idToken}`, true);
        var invitedRequest = this.$axios.delete(`users/${rootState.auth.idToken}/invited/${friendUuid}.json?auth=${rootState.auth.idToken}`, true);
        Promise.all([invitingRequest, invitedRequest]).catch((responses) => {
            console.log('friends::declineInvite::failed', responses);
            // @TODO Manage Error, may be juste display a toast
        });
        dispatch('user/fetchUser', null, {root:true});
    },

}

export const getters = {
    friendsProfile: (state) => {
        if (state.friends) {
            Object.keys(state.friends).map(friendUudi => state.friends[friendUudi])
        }
        return null;
    },
}