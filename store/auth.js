const AUTO_REFRESH_ANTICIPATION_IN_MS = 5000;
var refreshFunction = null;

export const state = () => ({
    idToken: null,//string	A Firebase Auth ID token for the newly created user.
    email: null,//string	The email for the newly created user.
    refreshToken: null,//string	A Firebase Auth refresh token for the newly created user.
    expiresAt: null,//string	The number of seconds in which the ID token expires.
    localId: null,//string	The uid of the newly created user.
});
  
export const mutations = {
    CONNECT(state, {idToken, email, refreshToken, expiresIn, localId}) {
        state.idToken = idToken;
        state.refreshToken = refreshToken;
        state.email = email;
        state.localId = localId;
        state.expiresAt = Date.now() + expiresIn;
        localStorage.auth = JSON.stringify(state);
    },
    REFRESH(state, {id_token, refresh_token, expires_in}){
        state.idToken = id_token;
        state.refreshToken = refresh_token;
        state.expiresAt = Date.now() + expiresIn;
        localStorage.auth = JSON.stringify(state);
    },
    DISCONNECT(state) {
        state.idToken = null;
        state.email = null;
        state.refreshToken = null;
        state.expiresAt = null;
        state.localId = null;
        localStorage.removeItem('store');
    }
}

export const actions = {
    signIn({commit, dispatch}, payload) {
        this.$axiosAuth.post('accounts:signUp?key=AIzaSyBaee2GXroL2tsPCAupQI_9BzuSAmH6BYk', {
            email: payload.email,
            password: payload.password,
            returnSecureToken: true,
        }).then(({data}) => {
            commit('CONNECT', data);
            payload.id = data.localId;
            console.log('auth::signIn::payload', payload)
            dispatch('user/createUser', payload, {root : true});
        }).catch((axios) => {
            commit('DISCONNECT');
            if (payload.callback) {
                var {response} = axios;
                console.log(axios);
                if (!response) {
                    payload.callback(['auth.error.signIn']);
                } else {
                    payload.callback(response.data.error.errors.map(error => `auth.error.signIn.${error.message}`));
                }
            }
        });
    },
    login({commit, dispatch}, payload) {        
        this.$axiosAuth.post('accounts:signInWithPassword?key=AIzaSyBaee2GXroL2tsPCAupQI_9BzuSAmH6BYk', {
            email: payload.email,
            password: payload.password,
            returnSecureToken: true,
        }).then(({data}) => {
            commit('CONNECT', data);
            dispatch('user/fetchUser', data, {root : true});
        }).catch(({errors}) => {
            commit('DISCONNECT');
            if (payload.callback) {
                payload.callback([`auth.error.login`]);
            }
        });
    },
    logout({commit}) {
        commit('DISCONNECT');
    },
    autoRefresh({dispatch, state, commit}) {
        if (!refreshFunction) {
            clearTimeout(refreshFunction);
        }
        var refreshInN = state.expiresAt - Date.now() - AUTO_REFRESH_ANTICIPATION_IN_MS;
        if (refreshInN > 0) {
            refreshFunction = setTimeout(() => {
                dispatch('refreshAuth');
            }, refreshInN);
        } else if (refreshInN > -AUTO_REFRESH_ANTICIPATION_IN_MS) {
            dispatch('refreshAuth');
        } else {
            commit('DISCONNECT');
        }
    },
    refreshAuth({commit, state}, payload) {
        this.$axiosAuth.post('token?key=AIzaSyBaee2GXroL2tsPCAupQI_9BzuSAmH6BYk', {
            grant_type: 'refresh_token',
            refresh_token: state.refreshToken,
        }).then((res) => {
            commit('REFRESH', res.data);
            console.log('REFRESH', res);
            if (payload && payload.callback) {
                payload.callback();
                // @TODO display a toast
            }
        }).catch(({errors}) => {
            commit('DISCONNECT');
            if (payload && payload.callback) {
                payload.callback([`auth.error.refresh`]);
            }
        });
    }
}

export const getters = {
    connected: (state) => state.idToken !== null,
}