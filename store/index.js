const storeToSaveInLocalStorage = [
    'auth',
    'user',
    'friends',
];

export const state = () => ({
    __initialized: false
});
  
export const mutations = {
    INITALIZE: function (state, initaleState) {
        console.log('state.__initialized', state.__initialized);
        if (!state.__initialized) {
            Object.assign(state, initaleState);
            state.__initialized = true;
        }
    }
}

export const actions = {
    loadStateFromLocaleStorage({ commit }, state) {
        var stateToLoad = storeToSaveInLocalStorage.reduce((acc, storeModule) => {
            try {
                acc[storeModule] = JSON.parse(localStorage.getItem(storeModule));
                return acc;
            } catch (e) {
                console.log('global::INITALIZE::module::catch', storeModule, e);
            }
        }, {});
        console.log('global::INITALIZE::before', stateToLoad);
        commit('INITALIZE', stateToLoad);
    },
    saveStateToLocaleStorage({ state }) {
        console.log('saveStateToLocaleStorage');
        storeToSaveInLocalStorage.forEach((storeModule) => {
            if (state[storeModule]) {
                localStorage.setItem(storeModule, JSON.stringify(state[storeModule]));
            }
        });
    }

}

export const getters = {

}