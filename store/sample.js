export const state = () => ({
    sampleCounter: 2
});
  
export const mutations = {
    INCREMENT(state) {
        state.sampleCounter++;
    }
}

export const actions = {
    sampleIncrement({commit}) {
        commit('INCREMENT');
    }
}

export const getters = {
    getSample: (state) => state.sampleCounter,
}