export const state = () => ({
    public: {
        username: null,
        avatar: null,
    },
    email: null,
    friends: {},
    invited: {},// You send a friend request
    inviting: {},// He/She send a friend request
    creationDate: null,
    lastUpdate: null,
});

export const mutations = {
    FETCH_USER(state, {response}) {
        state.lastUpdate = new Date();
        Object.keys(response).forEach((key) => state[key] = response[key]);
    },
    PUSH_USER(state) {
        state.lastUpdate = new Date();
    },
    CREATE_USER(state, {id, username, email}) {
        state.public.username = username;
        state.email = email;
        state.creationDate = new Date();
    }
}

export const actions = {
    createUser({commit, state, rootState}, {id, username, email,  callback}) {
        console.log('user::createUser');
        commit('CREATE_USER', {
            email,
            username,
        });
        this.$axios.post(`users.json?auth=${rootState.auth.idToken}`, state)
            .then((response) => {
                callback()
            }).catch(() => {
                if (callback) {
                    callback(['user.error.createUser']);
                }
            });
    },
    fetchUser({commit, state, rootState}, {callback}) {
        console.log('this.$store', this.$store);
        this.$axios.get(`users/${state.localId}.json?auth=${rootState.auth.idToken}`)
            .then((response) => {
                commit('FETCH_USE', response)
                callback();
            }).catch(() => {
                callback(['user.error.fetchUser']);
            });
    },
    syncUser({commit, state, rootState}, {callback}) {
        this.$axios.put(`users/${state.localId}.json?auth=${rootState.auth.idToken}`, state)
            .then((response) => {
                commit('PUSH_USER');
                callback()
            }).catch(() => {
                callback(['user.error.syncUser']);
            });
    },
}

export const getters = {
}