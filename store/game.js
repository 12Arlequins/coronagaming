export const state = () => ({
    games : {},
    lastUpdate : null,
});
  
export const mutations = {
    FETCH_GAMES(state, games) {
        state.games = games;
        state.lastUpdate = new Date();
    },
    PUSH_GAME(state, game) {

    }
}

export const actions = {
    fetchGames({ commit, rootState}) {
        console.log('game::fetchGames');
        this.$axios.get(`games/${state.localId}.json?auth=${rootState.auth.idToken}`)
        .then((response) => {
            commit('FETCH_GAMES', response)
            callback();
        }).catch(() => {
            callback(['game.error.fetchGames']);
        });
        commit('NAV_TIGGER_AUTH');
    }
}

export const getters = {
    showAuthenticationMenu: (state) => state.authenticationMenuVisible,
}