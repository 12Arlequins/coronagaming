import sha256 from 'crypto-js/sha256';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';

const crypto = {
    sha256 (message) {
        const hashDigest = sha256(message);
        return Base64.stringify(hashDigest);
    }
}

export default function ({}, inject) {
    inject('crypto', crypto);
}