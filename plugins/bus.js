import Vue from 'vue'

const eventBus = {}
/*
nav::singIn::click
nav::login::click
*/

eventBus.install = function (Vue) {
  Vue.prototype.$bus = new Vue()
}

Vue.use(eventBus)