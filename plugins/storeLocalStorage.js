export default function ({ app, store }) {
    if (process.browser) {
        store.dispatch('loadStateFromLocaleStorage');
        store.dispatch('auth/autoRefresh');
        store.subscribe(() => {
            store.dispatch('saveStateToLocaleStorage');
        });
    }
}