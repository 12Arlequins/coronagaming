export default function ({ $axios, redirect }, inject) {
    $axios.defaults.baseURL = 'https://corona-gaming-2020.firebaseio.com/';
    // Create a custom axios instance

    auth($axios, inject);
    images($axios, inject);
}

function auth($axios, inject) {
    const axiosAuth = $axios.create({
        baseURL: 'https://identitytoolkit.googleapis.com/v1/',
    });
    // AIzaSyBaee2GXroL2tsPCAupQI_9BzuSAmH6BYk
    // Inject to context as $axiosAuth
    inject('axiosAuth', axiosAuth);
}

function images($axios, inject) {
    const axiosImages = $axios.create({
        baseURL: 'gs://corona-gaming-2020.appspot.com/',
    });
    inject('axiosImages', axiosImages);
}
