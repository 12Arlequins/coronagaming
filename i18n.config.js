import fr from './lang/fr';
import en from './lang/en';

export default {
    strategy: 'prefix',
    locales: ['fr', 'en'],
    defaultLocale: 'fr',
    vueI18n: {
        fallbackLocale: 'en',
        messages: {
            en,
            fr
        }
    }
}